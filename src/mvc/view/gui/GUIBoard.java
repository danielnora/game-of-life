package mvc.view.gui;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import mvc.controller.GameController;
import observer.Observer;

@SuppressWarnings("serial")
public class GUIBoard extends JPanel implements Observer {

	private ImageIcon liveCell = new ImageIcon("live.jpg");
	private ImageIcon deadCell = new ImageIcon("dead.jpg");

	private int height;
	private int width;

	private GameController controller;
	private GUICell[][] theBoard;

	public GUIBoard(GameController controller) {
		super();
		height = controller.getBoardHeight();
		width = controller.getBoardWidth();
		this.controller = controller;

		createTheBoard();
		update();

		GridLayout manager = new GridLayout(height, width);
		setLayout(manager);

		for ( int i = 0; i < height; i++ ) {
			for ( int j = 0; j < width; j++ ) {
				add(theBoard[i][j]);
			}
		}

		setVisible(true);
	}

	private void createTheBoard() {
		theBoard = new GUICell[height][width];

		for ( int i = 0; i < height; i++ ) {
			for ( int j = 0; j < width; j++ ) {
				theBoard[i][j] = new GUICell(i, j);
				theBoard[i][j].setBackground(Color.white);
				theBoard[i][j].setBorderPainted(false);
				theBoard[i][j].addActionListener(new CellListener(theBoard[i][j]));
			}
		}
	}

	private class CellListener implements ActionListener {

		private GUICell button;

		public CellListener(GUICell button) {
			this.button = button;
		}

		//TODO: How to access such methods without explicit reference to controller?
		@Override
		public void actionPerformed(ActionEvent e) {
			if ( controller.isCellAlive(button.i, button.j) ) {
				button.setIcon(deadCell);
				controller.makeCellDead(button.i, button.j);
			} else {
				button.setIcon(liveCell);
				controller.makeCellAlive(button.i, button.j);
			}
		}

	}

	@Override
	public void update() {
		for ( int i = 0; i < height; i++ ) {
			for ( int j = 0; j < width; j++ ) {
				if ( controller.isCellAlive(i, j) )
					theBoard[i][j].setIcon(liveCell);
				else
					theBoard[i][j].setIcon(deadCell);
				theBoard[i][j].setVisible(true);
			}
		}
	}

}
