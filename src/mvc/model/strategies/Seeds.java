package mvc.model.strategies;

import mvc.model.GameStrategy;
/**
 * Seeds game strategy. Cells always die
 * in the next generation. It is, however
 * easier for a cell to ressurrect (only
 * two neighbors are required).
 * @author Miguel
 */
public class Seeds extends GameStrategy {

	public boolean shouldKeepAlive(int i, int j) {
		return false;
	}

	public boolean shouldRevive(int i, int j) {
		return (!engine.isCellAlive(i, j))
				&& (engine.numberOfNeighborhoodAliveCells(i, j) == 2);
	}

}
