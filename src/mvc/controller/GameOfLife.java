package mvc.controller;

import java.util.Scanner;

import mvc.model.GameEngine;
import mvc.model.Statistics;
import mvc.view.GameView;
import mvc.view.gui.GUIView;
import mvc.view.shell.ShellView;

public class GameOfLife {

	private GameEngine engine;
	private GameController controller;
	private GameView view;
	private Statistics statistics;

	private Scanner s = new Scanner(System.in);

	public GameOfLife() {
		engine = new GameEngine(getHeight(), getWidth());
		statistics = new Statistics(engine);
		controller = new GameController();
		controller.setStatistics(statistics);
		controller.setEngine(engine);
		view = selectGameView();
		view.start();
	}

	public int getHeight() {
		System.out.print("Enter the height of the board: ");
		int height = readPositiveNumber();
		if ( height <= 0 ) {
			System.out.println("You have to enter a positive number!");
			getHeight();
		}
		return height;
	}

	public int readPositiveNumber() {
		int number = 10;
		try {
			number = Integer.parseInt(s.nextLine());
		} catch (NumberFormatException e) {
			System.out.println("You have to enter a number!");
		}
		return number;
	}

	public int getWidth() {
		System.out.print("Enter the width of the board: ");
		int largura = readPositiveNumber();
		if ( largura <= 0 ) {
			System.out.println("You have to enter a positive number!");
			getHeight();
		}
		return largura;
	}

	public GameView selectGameView() {
		System.out.println("Please, select the view you'd like to use:");
		System.out.println("[1] CLI (Console)");
		System.out.println("[2] GUI (Swing)");
		System.out.print("Option: ");

		try {
			int view = Integer.parseInt(s.nextLine());
			if ((view != 1) && (view != 2)) {
				System.out.println("You have to choose a valid view!");
				selectGameView();
			}
			switch (view) {
			case 1: return new ShellView(controller, engine);
			case 2: return new GUIView(controller, engine);
			}
		} catch (NumberFormatException e) {
			System.out.println("You have to enter a number!");
			selectGameView();
		}

		return null; // Never happens
	}

}
