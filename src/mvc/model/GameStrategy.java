package mvc.model;


public abstract class GameStrategy {
	
	protected GameEngine engine;
	
	public abstract boolean shouldKeepAlive(int i, int j);
	public abstract boolean shouldRevive(int i, int j);
	
	public void setEngine(GameEngine engine) {
		this.engine = engine;
	}
	
}
