package mvc.view.gui;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import mvc.controller.GameController;
import mvc.model.GameEngine;
import mvc.view.GameView;

public class GUIView extends GameView implements Runnable {

	private GameWindow frame;
	private String statisticsMessage;

	public GUIView(GameController controller, GameEngine engine) {
		super(controller, engine);
		SwingUtilities.invokeLater(this);
	}

	/**
	 * update() method responsible for updating
	 * the board in the GUI whenever there are
	 * changes in the Model.
	 *
	 * It is also responsible for updating the
     * message that contains the statistics to
     * be displayed whenever the Model changes.
	 */
	@Override
	public void update() {
		frame.updateBoard();
		statisticsMessage = controller.getStatistics();
	}

	@Override
	public void start() {}

	@Override
	public void showStatistics() {
		JOptionPane.showMessageDialog(null, statisticsMessage);
	}

	@Override
	public void run() {
		frame = new GameWindow(controller);
	}

}
