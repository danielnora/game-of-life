package mvc.model;

/**
 * Cell representation for a Game of Life board.
 */
public class Cell {
	private boolean alive = false;

	public boolean isAlive() {
		return alive;
	}

	public void kill() {
		this.alive = false;
	}

	public void revive() {
		this.alive = true;
	}
}
